from fastapi import FastAPI
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

def test_spider_search():
    response = client.get("searchComics", params={"search_word": "spider"})
    assert response.status_code == 200