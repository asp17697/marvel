from pydantic import BaseModel, Field
from typing import Optional

class UserBaseModel(BaseModel):
    name: Optional[str]
    password: str
    username: str
    age: Optional[int]


class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    username: Optional[str] = None
