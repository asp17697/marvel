from requests import get
from datetime import timedelta, datetime
from fastapi import Depends, HTTPException, status
from .models import User, Comic, Layaway
from .basemodels import  TokenData
from jose import jwt,JWTError 
from .setting import SECRET_KEY, ALGORITHM, ACCESS_TOKEN_EXPIRE_MINUTES, crypt_context, oauth2_scheme
import json
import hashlib

public_key = '60ed9be68efc8fc08d0f178db0498032'
private_key = '2f8a1c2ae20461c7e6bfa3d741fc1c3a8677ebe3'


def marvel_request(word:str=None, searches_type:list=["characters", "comics"])->dict:
    headers = {"Content-Type": "application/json"}
    data = {}
    params = {"apikey": "60ed9be68efc8fc08d0f178db0498032"}
    searches_type = ["characters"] if word is None else searches_type
    for search_type in searches_type:
        ts = int(round(datetime.now().timestamp()))
        marvel_hash = hashlib.md5((f"{ts}{private_key}{public_key}").encode()).hexdigest()
        url = f"https://gateway.marvel.com/v1/public/{search_type}"
        character = search_type=="characters"

        params[(temp_key:= f"{'name' if character else 'title'}StartsWith")] = word
        params["ts"] = ts
        params["hash"] = marvel_hash
        response = get(url=url, params = params, headers = headers)
        params.pop(temp_key)
        if response.status_code == 200:
            data[search_type] = []
            for result in response.json()["data"]["results"]:
                temp_data = {
                    "id": result["id"],
                    "image": result["thumbnail"]["path"]
                }
                if character:
                    temp_data["name"] = result["name"]
                    temp_data["appearances"] = result["comics"]["available"]
                else:
                    temp_data["tittle"] = result["title"]
                    temp_data["onsaleDate"] = list(filter(None,[x["date"] if x["type"]=="onsaleDate" else None for x in result["dates"]]))[0]
                data[search_type].append(temp_data)                    
    return data

def get_password_hash(password):
    return crypt_context.hash(password)

def get_user(username: str):
    try:
        user = json.loads(User.objects.get(username=username).to_json())
        return user
    except User.DoesNotExist:
        return None

async def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("user")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = get_user(username=token_data.username)
    if user is None:
        raise credentials_exception
    return user

def verify_password(plain_password, hashed_password):
    return crypt_context.verify(plain_password, hashed_password)


def authenticate(username, password):
    try:
        user = get_user(username)
        password_check = verify_password(password, user['password'])
        return password_check
    except User.DoesNotExist:
        return False

def create_access_token(data: dict):
    to_encode = data.copy()
    to_encode.update({"exp": datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt

def getOrderLays(sorted_by, current_user):
    lays = Layaway.objects.filter(user=current_user)
    comics = []
    for lay in lays:
        total_comics = Comic.objects.filter(id_comic=lay.comic)
        for comic in total_comics:
            comics.append({
                "id_comic": comic.id_comic,
                "tittle": comic.title,
                "onsaleDate": comic.onsaleDate
            })

    comics.sort(key=lambda x: x[sorted_by])
    return comics