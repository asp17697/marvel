from datetime import datetime
from mongoengine import Document, StringField, DateTimeField, IntField, BooleanField, connect
from mongoengine import ObjectIdField

user = "admin"
password = "F0IVmyx4gzfIDoi1"
name = "MarvelTest"

db_uri = f"mongodb+srv://admin:{password}@marveltest.iaoud.mongodb.net/{name}?retryWrites=true&w=majority"
connect(host=db_uri)

class User(Document):
    _id = IntField()
    name = StringField(max_length=250, required=True)
    username = StringField(max_length=250, required=True)
    password = StringField(max_length=250, required=True)
    age = IntField(required=False)
    disabled = BooleanField(default=False)

class Comic(Document):
    _id = IntField()
    id_comic = IntField(required=True, unique=True)
    title = StringField(max_length=250, required=True)
    onsaleDate = StringField(max_length=250, required=False)

class Layaway(Document):
    _id = IntField()
    user = ObjectIdField(required=True)
    date_added = DateTimeField(default=datetime.utcnow)
    comic = IntField()
    #comic = MapField(EmbeddedDocumentField(Comic), required=False)
