from fastapi import FastAPI, Depends, HTTPException, Request
from typing import Optional

from .operations import marvel_request, getOrderLays
from .models import User, Comic, Layaway
from .basemodels import UserBaseModel, Token
from .operations import  authenticate, create_access_token, get_current_user, get_password_hash
from fastapi.security import OAuth2PasswordRequestForm

app = FastAPI(
    title="Marvel",
    version="0.0.1"
)

@app.options("/searchComics")
def SearchComicsOptions():
    doc = [{
        "method": "get",
        "parameters": {
            "search_word": {
                "type": "string",
                "required": False
            },
            "search_as": {
                "type": "string",
                "required": False,
                "choices": ["characters", "comics"]
            }
        }
    }]
    return doc

@app.get("/searchComics")
def SearchComics(search_word: Optional[str]=None, search_as: Optional[str]=None):
    if search_as not in ["characters", "comics", None]:
        raise HTTPException(status_code=400, detail="Search_as invalid. Available options are: characters and comics")
    
    response = marvel_request(search_word) if search_as is None else marvel_request(search_word, [search_as])
    return response

@app.options("/users")
def UserOptions():
    doc = [
        {
            "method": "post",
            "authorization required": False,
            "body fields": {
                "name": {
                    "type": "string",
                    "required": True,
                    "max digits": 250
                },
                "username": {
                    "type": "string",
                    "required": True,
                    "max digits": 250
                },
                "password": {
                    "type": "string",
                    "required": True,
                    "max digits": 250
                },
                "age": {
                    "type": "integer",
                    "required": False
                }
            }
        },
        {
            "method": "get",
            "authorization required": True,
            "parameters": None
        }
    ]
    return doc

@app.post("/users")
def sign_up(user: UserBaseModel):
    new_user = User(
        name = user.name,
        username = user.username, 
        password = get_password_hash(user.password),
        age=user.age)
    new_user.save()
    return {"message": "Created user successfully!"} 

@app.get("/users")
async def user_detail(request:Request, current_user: User = Depends(get_current_user)):
    print(request.headers)
    user_data = {
        "id": current_user["_id"]["$oid"],
        "name": current_user["name"],
        "age": current_user["age"],
        "token": request.headers.get('Authorization').split(" ")[1]
    }
    return user_data

@app.post("/token" , response_model=Token)
def login(form_data: OAuth2PasswordRequestForm = Depends()):
    username = form_data.username
    password = form_data.password
    if authenticate(username, password):
        access_token = create_access_token(data={"user": username})
        return {"access_token": access_token, "token_type": "bearer"}
    else:
        raise HTTPException(status_code=400, detail="Incorrect username or password")

@app.options("/addToLayaway/")
def addToLayawayOptions():
    doc = [
        {
            "method": "post",
            "authorization required": True,
            "parameters": {
                "comic_title": {
                    "type": "string",
                    "required": True
                }
            }
        }
    ]
    return doc

@app.post("/addToLayaway/")
def addToLayaway(comic_title: str, current_user: User = Depends(get_current_user)):
    comics = marvel_request(comic_title, ["comics"])["comics"]
    for comic in comics:
        lay_comic = Comic(
            id_comic = comic["id"],
            title = comic["tittle"],
            onsaleDate = comic["onsaleDate"]
        )
        try:
            lay_comic.save()
        except:
            pass
        lay = Layaway(
            user = current_user["_id"]["$oid"],
           comic = comic["id"]
        )
        lay.save() 
    return f"{len(comics)} comics saved"

@app.options("/getLayawayList")
def getLayOptions():
    doc = [
        {
            "method": "get",
            "authorization required": True,
            "parameters": {
                "sorted_by": {
                    "type": "string",
                    "required": False,
                    "choices": ["id_comic", "tittle", "onsaleDate"]
                }
            }
    }
    ]

@app.get("/getLayawayList")
def getLayaway(sorted_by: Optional[str]="id_comic", current_user: User = Depends(get_current_user)):
    if sorted_by not in ["id_comic", "tittle", "onsaleDate"]:
        raise HTTPException(status_code=400, detail="Incorrect sort_by input. Available options are: id_comi, title, onsaleDate")
    
    response = getOrderLays(sorted_by, current_user["_id"]["$oid"])
    return response

    
    

